#include <Wire.h>
#include "EspMQTTClient.h"
#include <string.h>
#include <LiquidCrystal_I2C.h>
#include <OpenTherm.h>
#include <Arduino.h>

/* Pins to connect the OpenTherm */
const int thermostatInPin = 12;   /* D7 */
const int thermostatOutPin = 13;  /* D6 */
OpenTherm ot(thermostatInPin, thermostatOutPin);

static float targetBoilerTemp = 1;
static float targetDHWTemp = 1;

/* Connect SDA, SDL to pins D2, D1 accordingly */
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

/* Change this accordingly to your private settings */
EspMQTTClient mqttClient(
  "ssid",
  "password",
  "homeassistan-ip-address",
  "mqtt-user",
  "mqtt-password",
  "thermostat-name"
);

const String topicBoilerTempTarget = "thermostat/boiler_temp/target";
const String topicBoilerTempActual = "thermostat/boiler_temp/actual";
const String topicDHWTempTarget = "thermostat/dhw_temp/target";

void setupLcd() {
  lcd.begin(16, 2);
  lcd.clear();
  lcd.print("Thermostat v1.0");
}

void ICACHE_RAM_ATTR handleInterrupt() {
  ot.handleInterrupt();
}

void setupOt() {
  ot.begin(handleInterrupt);
  ot.setDHWSetpoint(40);
}

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Smart Thermostat app for ESP8266.");

  setupLcd();
  setupOt();
}

static String lcdText[] = {
  "Boiler ?",
  "DW ?"
};

void lcdRefresh() {
  lcd.clear();
  lcd.print(lcdText[0].substring(0, 16));
  lcd.setCursor(0, 1);
  lcd.print(lcdText[1].substring(0, 16));
}

const String celcius = "C";

/* BT <actual temp>C <target temp>C */
void lcdSetBoilerTemp(float temp) {
  lcdText[0] = "BT " + String(temp, 1) + celcius + " " + String(targetBoilerTemp, 1) + celcius;
  lcdRefresh();
}

/* DW <actual temp>C <target temp>C */
void lcdSetDHWTemp(float temp) {
  lcdText[1] = "DW " + String(temp, 1) + celcius + " " + String(targetDHWTemp, 1) + celcius;
  lcdRefresh();
}

void onConnectionEstablished() {
  mqttClient.subscribe(topicDHWTempTarget, [] (const String &payload) {
    Serial.println("thermostat/dhw_temp: " + payload);
    targetDHWTemp = payload.toFloat();
  });

  mqttClient.subscribe(topicBoilerTempTarget, [] (const String &payload) {
    Serial.println("thermostat/boiler_temp/target: " + payload);
    targetBoilerTemp = payload.toFloat();
  });
}

void setBoilerConfigs() {
  bool enableCentralHeating = true;
  bool enableHotWater = true;
  bool enableCooling = false;
  unsigned long response = ot.setBoilerStatus(enableCentralHeating, enableHotWater, enableCooling);
  
  Serial.println("Central Heating: " + String(ot.isCentralHeatingActive(response) ? "on" : "off"));
  Serial.println("Hot Water: " + String(ot.isHotWaterActive(response) ? "on" : "off"));
  Serial.println("Flame: " + String(ot.isFlameOn(response) ? "on" : "off"));
  
  ot.setBoilerTemperature(targetBoilerTemp);
  ot.setDHWSetpoint(targetDHWTemp);
}

void getBoilerTemps() {
  float temperature = ot.getBoilerTemperature();
  Serial.println("Boiler temperature is " + String(temperature) + " degrees C");
  mqttClient.publish(topicBoilerTempActual, String(temperature, 1));
  lcdSetBoilerTemp(temperature);

  /* It does not work with our boiler, maybe it uses some older version of OT? */
  temperature = ot.getDHWTemperature();
  Serial.println("DHW temperature is " + String(temperature) + " degrees C");
  lcdSetDHWTemp(temperature);
}

/* We need some multitasking, mqtt client has to loop almost constantly,
   so we'll run thermostat functions rarerly. It shouldn't make a visible
   difference */
#define THERMOSTAT_DELAY_MS 1000
unsigned long previous_ts = millis();

void loop() {
  unsigned long current_ts = millis();
  mqttClient.loop();
  if (current_ts - previous_ts > THERMOSTAT_DELAY_MS) {
    setBoilerConfigs();
    getBoilerTemps();
    previous_ts = millis();
  }
}
