# THIS IS NOT THE MAIN README.MD OF THE PROJECT. If you want to read the main README then head to the [Home Assistant Configuration](https://gitlab.com/harmonyos-hackathon/submissions/team17/ha-configuration) page. 

This readme is dedicated to the thermostat-specific stuff.

# Smart OpenTherm thermostat

This thermostat is used with Home Assistant setup that is [described in great detail here](https://gitlab.com/harmonyos-hackathon/submissions/team17/ha-configuration). [OpenTherm](https://www.opentherm.eu/) is an open standard protocol for communicating with water-heating boilers.

This project is built with [Arduino software](https://www.arduino.cc/en/software/) and **uses open source libraries**:
- [OpenTherm library](https://github.com/ihormelnyk/opentherm_library) to communicate with the boiler.
- [ESP-MQTT library](https://github.com/plapointe6/EspMQTTClient) to communicate with the gateway.
- [New-LiquidCrystal](https://github.com/fmalpartida/New-LiquidCrystal) (to control the LCD).https://github.com/ihormelnyk/opentherm_library


## Setting up the board

First you need to get an adapter that enables to connect the NodeMcu to the boiler. [These are the schematis from Ihor Melnyk's page:](https://github.com/ihormelnyk/opentherm_library)
![](photos/opentherm_adapter_schematic.png)

We did it by assembling it on a breadboard, but we plan to soldere it in the future.

To install the software, first you need [to add the NodeMcu to your board manager in the Arduino IDE.](https://github.com/esp8266/Arduino).

Then simply flash the software and connect the wires.

## Connecting:
- LCD's SDA and SDL are connected to the D2, D1 pins on the NodeMcu. It needs a 5V voltage, so VCC should be connected to the VIN pin and the NodeMcu should be connected to a 5V power supply.
- Connect the IN wire to the D7 pin and OUT to the D6 pin. Connect the ground and voltage wires.

## Connecting to the Home Assistant

If you set up the HASS correctly and flashed the ESP with correct credentials, then it should automatically connect to the gateway.

## MQTT topics

Currently we support those topics:
- subscribed to *thermostat/boiler_temp/target* - gets target boiler temperature from HA
- publishing to *thermostat/boiler_temp/actual* - publishes the actual temperature of the boiler
- (needs fix) subscribed to *thermostat/dhw_temp/target* - gets target DHW (Domestic Hot Water) temperature.

## Plans for future
- We'd like to soldere the circuit and put it in a simple box, so there are no wires visible.
- Add more controllable options. OpenTherm is rich in many commands and it would be a waste to not utilize all of them

### Smart thermostat running on NodeMcu ESP8266.
![](photos/thermostat.jpeg)